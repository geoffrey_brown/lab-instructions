#include "ch.h"
#include "hal.h"
#include "sensor.h"


// magic i2c configuration -- processor/clock dependent

static const I2CConfig i2ccfg = {
    STM32_TIMINGR_PRESC(15U) |
    STM32_TIMINGR_SCLDEL(4U) |
    STM32_TIMINGR_SDADEL(2U) |
    STM32_TIMINGR_SCLH(15U) |
    STM32_TIMINGR_SCLL(21U),
    0, 0};

// hts221 driver configuration -- see ChibiOS documentation

const HTS221Config hts221cfg = {
  &I2CD1,
  &i2ccfg,
  NULL,
  NULL,
  NULL,
  NULL,
  HTS221_ODR_7HZ,
#if HTS221_USE_ADVANCED || defined(__DOXYGEN__)
  HTS221_BDU_CONTINUOUS,
  HTS221_AVGH_256,
  HTS221_AVGT_256
#endif
};

// write a block of data to i2c

int32_t i2c_sensor_write(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len) {
  uint8_t txbuf[32];
  i2c_sensor_t *sensor = handle;
  if (len > 11)
    return -10;
  txbuf[0] = reg;
  for (int i = 0; i < 15 && i < len; i++) {
    txbuf[i + 1] = bufp[i];
  }
  i2cAcquireBus(sensor->driver);
  int32_t retval =
  i2cMasterTransmitTimeout(sensor->driver, sensor->address,
				  txbuf, len + 1, 0, 0,
                                  sensor->timeout);
  i2cReleaseBus(sensor->driver);
  return retval;
}

// read a block of data from i2c -- uses Chibios Transmit routine
// which sends and then receives.

int32_t i2c_sensor_read(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len) {
  i2c_sensor_t *sensor = handle;
  i2cAcquireBus(sensor->driver);
  int32_t retval =  i2cMasterTransmitTimeout(sensor->driver, sensor->address,
			  &reg, 1, bufp, len, sensor->timeout);
  i2cReleaseBus(sensor->driver);
  return retval;
  
}

void i2c_sensor_init(void) {
  
  // configure pins, i2c device
  
  palSetLineMode(LINE_ARD_D15, PAL_MODE_ALTERNATE(4) |
                                   PAL_STM32_OSPEED_HIGH |
                                   PAL_STM32_OTYPE_OPENDRAIN);
  palSetLineMode(LINE_ARD_D14, PAL_MODE_ALTERNATE(4) |
                                   PAL_STM32_OSPEED_HIGH |
                                   PAL_STM32_OTYPE_OPENDRAIN);

  i2cStart(&I2CD1, &i2ccfg);
}
