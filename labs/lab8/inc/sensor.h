#ifndef __SENSOR_H__
#define __SENSOR_H__
#include "hts221.h"

typedef struct {
  I2CDriver *driver;
  sysinterval_t timeout;
  uint8_t  address;
} i2c_sensor_t;


extern const HTS221Config hts221cfg;

int32_t i2c_sensor_write(void *handle, uint8_t reg, uint8_t *bufp,
			 uint16_t len);

int32_t i2c_sensor_read(void *handle, uint8_t reg, uint8_t *bufp,
			uint16_t len);


void i2c_sensor_init(void);


#endif
