

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Darwin)
	OPENOCD := /usr/local/bin/openocd
	OCDSCRIPTS := /usr/local/share/openocd/scripts
	BOARDSCRIPT  := board/st_nucleo_l476rg.cfg
        CHIBIOS  := /common/cs/cs181u/ChibiOS
else
	OPENOCD := /usr/local/bin/openocd
	OCDSCRIPTS := /usr/local/share/openocd/scripts
	BOARDSCRIPT   := board/st_nucleo_l4.cfg
        CHIBIOS  := /home/geobrown/Software/ChibiOS
endif

