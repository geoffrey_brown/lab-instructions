# List of all the project related files.
PROJECTSRC = ./src/main.c ./src/lis2mdl_reg.c

# Required include directories
PROJECTINC = ./inc

# Shared variables
ALLCSRC += $(PROJECTSRC)
ALLINC  += $(PROJECTINC)
