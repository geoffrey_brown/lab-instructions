#include "ch.h"
#include "hal.h"
#include "chprintf.h"
#include "lis2mdl_reg.h"

#define LIS2MDL_TIMEOUT OSAL_MS2I(50)
#define LIS2MDL_ADDRESS (0x3C>>1)
static BaseSequentialStream* chp = (BaseSequentialStream*)&SD2;
#define cls(chp)  chprintf(chp, "\033[2J\033[1;1H")

static const I2CConfig i2ccfg = {
    STM32_TIMINGR_PRESC(15U) | 
    STM32_TIMINGR_SCLDEL(4U) |
    STM32_TIMINGR_SDADEL(2U) | 
    STM32_TIMINGR_SCLH(15U) |
    STM32_TIMINGR_SCLL(21U),
    0, 0};


typedef union{
  int16_t i16bit[3];
  uint8_t u8bit[6];
} axis3bit16_t;

typedef union{
  int16_t i16bit;
  uint8_t u8bit[2];
} axis1bit16_t;

/****************************************************************/
/*     ST Driver Hardware Abstraction Layer                     */
/****************************************************************/

typedef struct {
  I2CDriver *driver;
  uint8_t  address;
} i2c_sensor_t;

static i2c_sensor_t lis2mdl_handle = { &I2CD1, LIS2MDL_ADDRESS };

int32_t lis2mdl_write(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len) {
  uint8_t txbuf[16];
  i2c_sensor_t *sensor = handle;
  if (len > 15)
    return -10;
  txbuf[0] = reg;
  for (int i = 0; i < 15 && i < len; i++) {
    txbuf[i + 1] = bufp[i];
  }
  return i2cMasterTransmitTimeout(sensor->driver, sensor->address,
				  txbuf, len + 1, 0, 0, LIS2MDL_TIMEOUT);
}

int32_t lis2mdl_read(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len) {
  i2c_sensor_t *sensor = handle;
  return  i2cMasterTransmitTimeout(sensor->driver, sensor->address, &reg, 1,
				   bufp, len, LIS2MDL_TIMEOUT);
}

static axis3bit16_t data_raw_magnetic;
static axis1bit16_t data_raw_temperature;
static float magnetic_mG[3];
static float temperature_degC;
static uint8_t whoamI, rst;

stmdev_ctx_t dev_ctx = {
  .write_reg = lis2mdl_write,
  .read_reg = lis2mdl_read,
  .handle = &lis2mdl_handle
};


/*
 * Green LED blinker thread, times are in milliseconds.
 */
static THD_WORKING_AREA(waThread1, 256);
static THD_FUNCTION(Thread1, arg) {

  (void)arg;
  chRegSetThreadName("blinker");
  while (true) {
    palClearPad(GPIOA, GPIOA_LED_GREEN);
    chThdSleepMilliseconds(500);
    palSetPad(GPIOA, GPIOA_LED_GREEN);
    chThdSleepMilliseconds(500);
  }
}

/*
 * Application entry point.
 */
int main(void) {

  // system initialization

  halInit();
  chSysInit();

  // configure pins, i2c device
  
  palSetLineMode(LINE_ARD_D15, PAL_MODE_ALTERNATE(4) |
                                   PAL_STM32_OSPEED_HIGH |
                                   PAL_STM32_OTYPE_OPENDRAIN);
  palSetLineMode(LINE_ARD_D14, PAL_MODE_ALTERNATE(4) |
                                   PAL_STM32_OSPEED_HIGH |
                                   PAL_STM32_OTYPE_OPENDRAIN);


  i2cStart(&I2CD1, &i2ccfg);  // start i2c driver
  sdStart(&SD2, NULL);        // start serial driver

  // create blinkder thread

  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);

  // sanity check the lis2mdl device

  lis2mdl_device_id_get(&dev_ctx, &whoamI);
  if (whoamI != LIS2MDL_ID) {
    chprintf(chp,"Who am I read failed\n\r");
  }

  // reset lis2mdl to default configuration

  lis2mdl_reset_set(&dev_ctx, PROPERTY_ENABLE);
  do {
    lis2mdl_reset_get(&dev_ctx, &rst);
  } while (rst);

  
  // enable block data 
  lis2mdl_block_data_update_set(&dev_ctx, PROPERTY_ENABLE);  // block data

  // 10hz data rate
  lis2mdl_data_rate_set(&dev_ctx, LIS2MDL_ODR_10Hz);

  // set reset mode
  lis2mdl_set_rst_mode_set(&dev_ctx, LIS2MDL_SENS_OFF_CANC_EVERY_ODR);

  // enable temperature compensation
  lis2mdl_offset_temp_comp_set(&dev_ctx, PROPERTY_ENABLE);

  // set device in continuous mode

  lis2mdl_operating_mode_set(&dev_ctx, LIS2MDL_CONTINUOUS_MODE);
 
  while (true) {
    uint8_t reg;

    // check if data are ready
    lis2mdl_mag_data_ready_get(&dev_ctx, &reg);
    if (reg) {

      // read the magnetic field raw data

      lis2mdl_magnetic_raw_get(&dev_ctx, data_raw_magnetic.u8bit);
      magnetic_mG[0] = lis2mdl_from_lsb_to_mgauss(data_raw_magnetic.i16bit[0]);
      magnetic_mG[1] = lis2mdl_from_lsb_to_mgauss(data_raw_magnetic.i16bit[1]);
      magnetic_mG[2] = lis2mdl_from_lsb_to_mgauss(data_raw_magnetic.i16bit[2]);
     
      chprintf(chp, "Magnetic field [mG]:  %4.2f\t%4.2f\t%4.2f\r\n",
              magnetic_mG[0], magnetic_mG[1], magnetic_mG[2]);
     
      // read the temperature data

      lis2mdl_temperature_raw_get(&dev_ctx, data_raw_temperature.u8bit);
      temperature_degC = lis2mdl_from_lsb_to_celsius(data_raw_temperature.i16bit);
      
      chprintf(chp, "Temperature [degC]:%6.2f\r\n", temperature_degC);
    }

    chThdSleepMilliseconds(1000);
    //cls(chp);
  }
}
