#include "ch.h"
#include "hal.h"
#include "chprintf.h"
#include "lis2dw12_reg.h"

#define LIS2DW12_TIMEOUT OSAL_MS2I(50)
#define LIS2DW12_ADDRESS (0x32>>1)
static BaseSequentialStream* chp = (BaseSequentialStream*)&SD2;
#define cls(chp)  chprintf(chp, "\033[2J\033[1;1H")

// i2c communication interface

static const I2CConfig i2ccfg = {
    STM32_TIMINGR_PRESC(15U) | 
    STM32_TIMINGR_SCLDEL(4U) |
    STM32_TIMINGR_SDADEL(2U) | 
    STM32_TIMINGR_SCLH(15U) |
    STM32_TIMINGR_SCLL(21U),
    0, 0};

typedef struct {
  I2CDriver *driver;
  uint8_t  address;
} i2c_sensor_t;

static i2c_sensor_t lis2dw12_handle = { &I2CD1, LIS2DW12_ADDRESS };


int32_t lis2dw12_write(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len) {
  uint8_t txbuf[16];
  i2c_sensor_t *sensor = handle;
  if (len > 15)
    return -10;
  txbuf[0] = reg;
  for (int i = 0; i < 15 && i < len; i++) {
    txbuf[i + 1] = bufp[i];
  }
  return i2cMasterTransmitTimeout(sensor->driver, sensor->address,
				  txbuf, len + 1, 0, 0, LIS2DW12_TIMEOUT);
}

int32_t lis2dw12_read(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len) {
  i2c_sensor_t *sensor = handle;
  return  i2cMasterTransmitTimeout(sensor->driver, sensor->address, &reg, 1,
				   bufp, len, LIS2DW12_TIMEOUT);
}

stmdev_ctx_t dev_ctx = {
  .write_reg = lis2dw12_write,
  .read_reg = lis2dw12_read,
  .handle = &lis2dw12_handle
};

// union type for reading accelerometer data

typedef union{
  int16_t i16bit[3];
  uint8_t u8bit[6];
} axis3bit16_t;

static uint8_t whoamI, rst;

// activity thread
//    blinks led when wakeup detected

static THD_WORKING_AREA(waThreadActivity, 256);
static THD_FUNCTION(ThreadActivity, arg)
{
  (void)arg;
  lis2dw12_all_sources_t val;
  int active = 0;
  palEnablePadEvent(GPIOB, 0U, PAL_EVENT_MODE_RISING_EDGE);
  while (1) {
    palWaitPadTimeout(GPIOB, 0, TIME_INFINITE);
    palSetLine(LINE_LED_GREEN);
    chThdSleepMilliseconds(200);
    palClearLine(LINE_LED_GREEN);
    
  }
}

int main(void) {

  // initialize system

  halInit();
  chSysInit();

  // configure pins, i2c device
  
  palSetLineMode(LINE_ARD_D15, PAL_MODE_ALTERNATE(4) |
                                   PAL_STM32_OSPEED_HIGH |
                                   PAL_STM32_OTYPE_OPENDRAIN);
  palSetLineMode(LINE_ARD_D14, PAL_MODE_ALTERNATE(4) |
                                   PAL_STM32_OSPEED_HIGH |
                                   PAL_STM32_OTYPE_OPENDRAIN);

  // start i2c driver

  i2cStart(&I2CD1, &i2ccfg);

  // start serial driver

  sdStart(&SD2, NULL);

  // start activity detector

  palSetPadMode(GPIOB, 0, PAL_MODE_INPUT);

  chThdCreateStatic(waThreadActivity, sizeof(waThreadActivity), 
                    NORMALPRIO, ThreadActivity, NULL);


  // sanity check device

  whoamI = 0;
  lis2dw12_device_id_get(&dev_ctx, &whoamI);
  if ( whoamI != LIS2DW12_ID ) {
    chprintf(chp,"Who am I read failed\n\r");
  } else {
    chprintf(chp,"who am i %x\n\r", whoamI);
   }
    
  // reset device

  lis2dw12_reset_set(&dev_ctx, PROPERTY_ENABLE);
  do {
    lis2dw12_reset_get(&dev_ctx, &rst);
  } while (rst);

  // enable block data

  lis2dw12_block_data_update_set(&dev_ctx, PROPERTY_ENABLE);

  // set +- 2 g range

  lis2dw12_full_scale_set(&dev_ctx, LIS2DW12_2g);

  // configure filter -- low pass out (discard high frequency data)
  //                     knee is sample frequency/4

  lis2dw12_filter_path_set(&dev_ctx, LIS2DW12_LPF_ON_OUT);
  lis2dw12_filter_bandwidth_set(&dev_ctx, LIS2DW12_ODR_DIV_4);

  // high performance measurements  
  lis2dw12_power_mode_set(&dev_ctx, LIS2DW12_HIGH_PERFORMANCE);
  //lis2dw12_power_mode_set(&dev_ctx, LIS2DW12_CONT_LOW_PWR_LOW_NOISE_12bit);

  // set wake-up duration
  //     1LSb = 1/sample rate
  
  lis2dw12_wkup_dur_set(&dev_ctx, 2);

  // set sleep duration
  //     1LSb = 1/sample rate

  //lis2dw12_act_sleep_dur_set(&dev_ctx, 6);

  // set activity wake-up threshold
  // 1 LSb = FS_XL/64

  lis2dw12_wkup_threshold_set(&dev_ctx, 2);

  // send data to wake-up interrupt function

  lis2dw12_wkup_feed_data_set(&dev_ctx, LIS2DW12_HP_FEED);

  // configure activity/inactivity

  lis2dw12_act_mode_set(&dev_ctx, LIS2DW12_DETECT_ACT_INACT);

  // select wakeup interrupt

  lis2dw12_reg_t int_route;
  
  lis2dw12_pin_int1_route_get(&dev_ctx, &int_route.ctrl4_int1_pad_ctrl);
  int_route.ctrl4_int1_pad_ctrl.int1_wu = PROPERTY_ENABLE;
  lis2dw12_pin_int1_route_set(&dev_ctx, &int_route.ctrl4_int1_pad_ctrl);

  // set sample rate -- 12.5 hz.

  lis2dw12_data_rate_set(&dev_ctx, LIS2DW12_XL_ODR_25Hz);

  // enable interrupt

  lis2dw12_read_reg(&dev_ctx, LIS2DW12_CTRL_REG7, (uint8_t *) &int_route.ctrl_reg7, 1);
  int_route.ctrl_reg7.interrupts_enable = PROPERTY_ENABLE;
  lis2dw12_write_reg(&dev_ctx, LIS2DW12_CTRL_REG7, (uint8_t *) &int_route.ctrl_reg7, 1);

  int active = 0;
  while (true) {
    lis2dw12_all_sources_t all_source;
  
    // read status

    lis2dw12_all_sources_get(&dev_ctx, &all_source);

    if (all_source.wake_up_src.sleep_state_ia) {
      if (active)
        chprintf(chp, "Inactivity Detected\r\n");
      active = 0;
    }

    if (all_source.wake_up_src.wu_ia){
      if (!active)
        chprintf(chp, "Activity Detected\r\n");
      active = 1;
    }

    chThdSleepMilliseconds(10);
    //cls(chp);
  }
}
