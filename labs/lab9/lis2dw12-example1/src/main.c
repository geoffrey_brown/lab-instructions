#include "ch.h"
#include "hal.h"
#include "chprintf.h"
#include "lis2dw12_reg.h"

#define LIS2DW12_TIMEOUT OSAL_MS2I(50)
#define LIS2DW12_ADDRESS (0x32>>1)
static BaseSequentialStream* chp = (BaseSequentialStream*)&SD2;
#define cls(chp)  chprintf(chp, "\033[2J\033[1;1H")

// i2c communication interface

static const I2CConfig i2ccfg = {
    STM32_TIMINGR_PRESC(15U) | 
    STM32_TIMINGR_SCLDEL(4U) |
    STM32_TIMINGR_SDADEL(2U) | 
    STM32_TIMINGR_SCLH(15U) |
    STM32_TIMINGR_SCLL(21U),
    0, 0};

typedef struct {
  I2CDriver *driver;
  uint8_t  address;
} i2c_sensor_t;

static i2c_sensor_t lis2dw12_handle = { &I2CD1, LIS2DW12_ADDRESS };


int32_t lis2dw12_write(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len) {
  uint8_t txbuf[16];
  i2c_sensor_t *sensor = handle;
  if (len > 15)
    return -10;
  txbuf[0] = reg;
  for (int i = 0; i < 15 && i < len; i++) {
    txbuf[i + 1] = bufp[i];
  }
  return i2cMasterTransmitTimeout(sensor->driver, sensor->address,
				  txbuf, len + 1, 0, 0, LIS2DW12_TIMEOUT);
}

int32_t lis2dw12_read(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len) {
  i2c_sensor_t *sensor = handle;
  return  i2cMasterTransmitTimeout(sensor->driver, sensor->address, &reg, 1,
				   bufp, len, LIS2DW12_TIMEOUT);
}

stmdev_ctx_t dev_ctx = {
  .write_reg = lis2dw12_write,
  .read_reg = lis2dw12_read,
  .handle = &lis2dw12_handle
};

// union type for reading accelerometer data

typedef union{
  int16_t i16bit[3];
  uint8_t u8bit[6];
} axis3bit16_t;

// local variables for accelerometer read data

static axis3bit16_t data_raw_acceleration;
static float acceleration_mg[3];
static uint8_t whoamI, rst;

// blinking led thread

static THD_WORKING_AREA(waThread1, 256);
static THD_FUNCTION(Thread1, arg) {

  (void)arg;
  chRegSetThreadName("blinker");
  while (true) {
    palClearPad(GPIOA, GPIOA_LED_GREEN);
    chThdSleepMilliseconds(500);
    palSetPad(GPIOA, GPIOA_LED_GREEN);
    chThdSleepMilliseconds(500);
  }
}

int main(void) {

  // initialize system

  halInit();
  chSysInit();

  // configure pins, i2c device
  
  palSetLineMode(LINE_ARD_D15, PAL_MODE_ALTERNATE(4) |
                                   PAL_STM32_OSPEED_HIGH |
                                   PAL_STM32_OTYPE_OPENDRAIN);
  palSetLineMode(LINE_ARD_D14, PAL_MODE_ALTERNATE(4) |
                                   PAL_STM32_OSPEED_HIGH |
                                   PAL_STM32_OTYPE_OPENDRAIN);

  // start i2c driver

  i2cStart(&I2CD1, &i2ccfg);

  // start serial driver

  sdStart(&SD2, NULL);

  // start blinker

  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);

  // sanity check device

  whoamI = 0;
  lis2dw12_device_id_get(&dev_ctx, &whoamI);
  if ( whoamI != LIS2DW12_ID ) {
    chprintf(chp,"Who am I read failed\n\r");
  } else {
    chprintf(chp,"who am i %x\n\r", whoamI);
   }
    
  // reset device

  lis2dw12_reset_set(&dev_ctx, PROPERTY_ENABLE);
  do {
    lis2dw12_reset_get(&dev_ctx, &rst);
  } while (rst);

  // enable block data

  lis2dw12_block_data_update_set(&dev_ctx, PROPERTY_ENABLE);

  // set +- 2 g range

  lis2dw12_full_scale_set(&dev_ctx, LIS2DW12_2g);

  // configure filter -- low pass out (discard high frequency data)
  //                     knee is sample frequency/4

  lis2dw12_filter_path_set(&dev_ctx, LIS2DW12_LPF_ON_OUT);
  lis2dw12_filter_bandwidth_set(&dev_ctx, LIS2DW12_ODR_DIV_4);

  // high performance measurements  
  lis2dw12_power_mode_set(&dev_ctx, LIS2DW12_HIGH_PERFORMANCE);
  //lis2dw12_power_mode_set(&dev_ctx, LIS2DW12_CONT_LOW_PWR_LOW_NOISE_12bit);

  // set sample rate -- 12.5 hz.

  lis2dw12_data_rate_set(&dev_ctx, LIS2DW12_XL_ODR_12Hz5);

  while (true) {
    
    uint8_t reg;

    // wait for new data 

    lis2dw12_flag_data_ready_get(&dev_ctx, &reg);
    if (reg) {
      // read data
      lis2dw12_acceleration_raw_get(&dev_ctx, data_raw_acceleration.u8bit);
      acceleration_mg[0] = lis2dw12_from_fs2_to_mg(data_raw_acceleration.i16bit[0]);
      acceleration_mg[1] = lis2dw12_from_fs2_to_mg(data_raw_acceleration.i16bit[1]);
      acceleration_mg[2] = lis2dw12_from_fs2_to_mg(data_raw_acceleration.i16bit[2]);
     
      chprintf(chp, "Acceleration [mg]:%4.2f\t%4.2f\t%4.2f\r\n",
              acceleration_mg[0], acceleration_mg[1], acceleration_mg[2]);
    }

    chThdSleepMilliseconds(500);
    //cls(chp);
  }
}
