#include <stdio.h>
#include <ctype.h>

int main(int argc, char *argv[]) {
    int charcnt = 0;
    int wordcnt = 0;
    int linecnt = 0;
    int inword = 0;
    int c;
    char *filename = "";
    FILE *file;
    if (!(argc > 1) || !(file = fopen(argv[1],"r"))) {
        file = stdin;
    } else {
        filename = argv[1];
    }
    
    while ((c = getc(file)) != EOF) {
        charcnt++;
        if (c == '\n')
            linecnt++;
        if (!isblank(c))
            wordcnt += inword ? 0 : 1;
        inword = !isblank(c);
    }
    printf(" %d %d %d %s\n", linecnt, wordcnt, charcnt, filename);
}