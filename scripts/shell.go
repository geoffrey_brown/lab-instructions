// simple serial tee in go
// opens a port, echoes stdin to the port and echoes the port
// to stdout

package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
	"time"

	"github.com/tarm/serial"
)

// write from stdin to serial port

func writeserial(s1 *serial.Port, done chan int) {
	// create scanner to read input

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		buf := []byte(scanner.Text())
		// have to throttle writes to prevent usb serial port
		// from hanging
		for i, _ := range buf {
			s1.Write(buf[i : i+1])
			time.Sleep(time.Millisecond)
		}
		s1.Write([]byte("\n"))
	}
	time.Sleep(time.Millisecond * 200)
	done <- 1
}

func main() {
	// read options
	var usbtty string
	if runtime.GOOS == "linux" {
		usbtty = "/dev/ttyACM0"
	}
	if runtime.GOOS == "darwin" {
		usbtty = "/dev/tty.usbmodem14603"
	}
	portPtr := flag.String("port", usbtty, "name of the port")
	baudPtr := flag.Int("baud", 115200, "the baud rate")
	flag.Parse()

	// open port with configuration
	// timeout on read to give target
	// a final chance to respond

	c1 := &serial.Config{Name: *portPtr, Baud: *baudPtr,
		ReadTimeout: time.Millisecond * 100}
	s1, err := serial.OpenPort(c1)
	if err != nil {
		log.Fatal(err)
	}

	// launch reader/writer threads

	done := make(chan int)
	go writeserial(s1, done)

	buf := make([]byte, 1)
loop:
	for {
		select {
		case <-done:
			//log.Println("got done !")
			break loop
		default:
			n, _ := s1.Read(buf)
			if n != 0 {
				fmt.Printf(string(buf[:n]))
			} else {
				//log.Println("Timeout")
			}
		}

	}
	//log.Println("exiting")
}
